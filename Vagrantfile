# -*- mode: ruby -*-
# vi: set ft=ruby :

infra = {
    "mysql" => {:ip => "192.168.33.12", :cpus => 2, :mem => 4096 },
    "wordpress" => {:ip => "192.168.33.11", :cpus => 2, :mem => 4096 },
    "elk-1" => {:ip => "192.168.33.21", :cpus => 4, :mem => 16384 },
    "elk-2" => {:ip => "192.168.33.22", :cpus => 4, :mem => 16384 },
    "elk-3" => {:ip => "192.168.33.23", :cpus => 4, :mem => 16384 },
    "fleet" => {:ip => "192.168.33.20", :cpus => 2, :mem => 4096 },
    "logstash" => {:ip => "192.168.33.24", :cpus => 2, :mem => 4096 },
    "kibana" => {:ip => "192.168.33.25", :cpus => 2, :mem => 2048 }
}

Vagrant.configure("2") do |config|
    config.vm.box = "generic/ubuntu2204"

    infra.each_with_index do |(hostname, info), index|

        config.vm.define hostname do |n|
            n.vm.network "private_network", ip: "#{info[:ip]}"
            n.vm.hostname = hostname

            n.vm.provider :libvirt do |domain|
                domain.memory = info[:mem]
                domain.cpus = info[:cpus]
			end

            if (hostname == "kibana")
                n.vm.provision "ansible", type: "ansible", run: "never" do |ansible|
                    ansible.limit = "all"
                    ansible.playbook = "playbook.yml"
                    ansible.galaxy_role_file = "requirements.yml"
                    ansible.become = true
                    ansible.groups = {
                      "web" => ["wordpress"],
                      "bdd" => ["mysql"],
                      "elasticsearch_master" => ["elk-[1:3]"],
                      "elasticsearch_data" => [],
                      "elasticsearch_ca" => ["elk-1"],
                      "kibana" => ["kibana"],
                      "logstash" => ["logstash"],
                      "logstash-beats" => ["wordpress"],
                      "fleet" => ["fleet"],
                      "elasticsearch:children" => ["elasticsearch_master", "elasticsearch_data"],
                      "elasticsearch:vars" => {"generateca" => true},
                      "all_groups:children" => ["web", "bdd", "kibana", "logstash", "elasticsearch", "fleet"]
                    }
                end
            end
        end
    end

    config.vm.provision "provision", type: "shell", privileged: true, run: "once", inline: <<-SHELL
    if [ ! -f ~/runonce ];then   
        # System
        apt-get update
        timedatectl set-timezone Europe/Paris
        # Hosts
        echo "192.168.33.11 wordpress.lab wordpress" >> /etc/hosts
        echo "192.168.33.12 mysql.lab mysql" >> /etc/hosts
        echo "192.168.33.20 fleet.lab fleet" >> /etc/hosts
        echo "192.168.33.21 ELK-1.lab ELK-1" >> /etc/hosts
        echo "192.168.33.22 ELK-2.lab ELK-2" >> /etc/hosts
        echo "192.168.33.23 ELK-3.lab ELK-3" >> /etc/hosts
        echo "192.168.33.24 logstash.lab logstash" >> /etc/hosts
        echo "192.168.33.25 kibana.lab kibana" >> /etc/hosts
        # Vagrant
        echo "export TERM=xterm" >> /home/vagrant/.bashrc
        touch ~/runonce
    fi
    SHELL
end
